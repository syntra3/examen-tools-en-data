"""
Opdracht:

Bepalingen:
 - Je moet gebruik maken van de aangeleverde variable(n)
 - Je mag deze variable(n) niet aanpassen
 - Het is de bedoeling dat je op het einde 1 programma hebt
 - Als inlever formaat wordt een public git url verwacht die gecloned kan worden

/ 5 ptn 1 - Maak een public repository aan op jouw gitlab/github account voor dit project X
/10 ptn 2 - Gebruik python om de gegeven joke_url aan te spreken X
/10 ptn 3 - Gebruik regex om de volgende data te extracten:
            - De eerste 20 tekens van de joke
/15 ptn 4 - Verzamel onderstaande data en output alles als yaml. Een voorbeeld vind je hieronder.
/10 ptn 5 - Doe een post naar de post_url.
            - Stuur in de post een parameter genaamd "naam" mee met als waarde jouw voornaam
            - Output het hele antwoord naar de terminal
            - Haal uit het antwoord van de call jouw ip adres (origin) en geef dit weer met een f string

Totaal  /50ptn
"""

""" voorbeeld yaml output
jokes:
  - intro: <met regex extracted intro>
    joke: <joke>
    id: <joke id>
  - .. <tot aantal jokes bereikt is>
"""

import requests
import re
import yaml
import dotenv

env = dotenv.dotenv_values()

# joke_url = "https://icanhazdadjoke.com"
amount_of_jokes = 5

# https://httpbin.org/#/HTTP_Methods/post_post
# post_url = "https://httpbin.org/post"

headers = {"Accept": "application/json"}

response = requests.get(env['JOKE_URL'], headers=headers)
joke_data = response.json()
# print(joke_data)

jokes = []
for joke in range(amount_of_jokes):
    response = requests.get(env['JOKE_URL'], headers=headers)
    joke_data = response.json()
    joke_intro = re.match(r'(.{20})', joke_data['joke']).group()
    jokes.append({
        
        'intro': joke_intro,
        'joke': joke_data['joke'],
        'id': joke_data['id']
        
    })

output_data = {'jokes': jokes}
yaml_output = yaml.dump(output_data)
print(yaml_output)


# Doe een post naar de post_url.
post_json = {
  "naam": "Vinnie"
  }
post_response = requests.post(env['POST_URL'], json=post_json)
print(post_response.text)

origin_ip = post_response.json()['origin']
print(f"Your origin IP address: {origin_ip}")